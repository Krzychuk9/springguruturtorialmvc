package pl.kasprowski.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.configurations.JPATestConfiguration;
import pl.kasprowski.domain.*;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JPATestConfiguration.class)
@ActiveProfiles("jpadao")
public class UserServiceJpaDaoImplTest {

    @Autowired
    private UserService userService;
    @Autowired
    private ProductService productService;

    @Test
    public void testSaveUser() throws Exception {
        User user = new User();
        user.setPassword("password");
        user.setUsername("userName");

        User returnedUser = userService.saveOrUpdate(user);

        assertThat(returnedUser.getId(), is(notNullValue()));
        assertThat(returnedUser.getEncryptedPassword(), is(notNullValue()));
    }

    @Test
    public void testSaveOFUserWithCustomer() throws Exception {
        User user = new User();
        user.setPassword("password");
        user.setUsername("userName");

        Customer customer = new Customer();
        customer.setFirstName("firstName");
        customer.setLastName("lastName");

        user.setCustomer(customer);

        User savedUser = userService.saveOrUpdate(user);

        assertThat(savedUser.getId(), is(notNullValue()));
        assertThat(savedUser.getVersion(), is(notNullValue()));
        assertThat(savedUser.getCustomer(), is(notNullValue()));
        assertThat(savedUser.getCustomer().getId(), is(notNullValue()));
    }

    @Test
    public void testAddCartToUser() throws Exception {
        User user = new User();
        user.setPassword("password");
        user.setUsername("userName");

        user.setCart(new Cart());

        User savedUser = userService.saveOrUpdate(user);

        assertThat(savedUser.getId(), is(notNullValue()));
        assertThat(savedUser.getVersion(), is(notNullValue()));
        assertThat(savedUser.getCart(), is(notNullValue()));
        assertThat(savedUser.getCart().getId(), is(notNullValue()));
    }

    @Test
    public void testAddCartToUserWithCartDetails() throws Exception {
        User user = new User();
        user.setPassword("password");
        user.setUsername("userName");

        user.setCart(new Cart());

        List<Product> products = (List<Product>) productService.getAll();

        CartDetails cartItemOne = new CartDetails();
        cartItemOne.setProduct(products.get(0));
        cartItemOne.setQuantity(2);
        user.getCart().addCartDetails(cartItemOne);

        CartDetails cartItemTwo = new CartDetails();
        cartItemTwo.setProduct(products.get(1));
        cartItemTwo.setQuantity(3);
        user.getCart().addCartDetails(cartItemTwo);

        User savedUser = userService.saveOrUpdate(user);

        assertThat(savedUser.getId(), is(notNullValue()));
        assertThat(savedUser.getVersion(), is(notNullValue()));
        assertThat(savedUser.getCart(), is(notNullValue()));
        assertThat(savedUser.getCart().getId(), is(notNullValue()));
        assertThat(savedUser.getCart().getCartDetails(), hasSize(2));
        assertThat(savedUser.getCart().getCartDetails().get(1).getQuantity(), is(equalTo(3)));
    }

    @Test
    public void testAddAndRemoveCartToUserWithCartDetails() throws Exception {
        List<Product> products = (List<Product>) productService.getAll();
        User user = new User();
        user.setPassword("password");
        user.setUsername("userName");
        user.setCart(new Cart());

        CartDetails cartItemOne = new CartDetails();
        cartItemOne.setProduct(products.get(0));
        user.getCart().addCartDetails(cartItemOne);

        CartDetails cartItemTwo = new CartDetails();
        cartItemTwo.setProduct(products.get(1));
        user.getCart().addCartDetails(cartItemTwo);

        User savedUser = userService.saveOrUpdate(user);

        assertThat(savedUser.getCart().getCartDetails(), hasSize(2));

        savedUser.getCart().removeCartDetails(savedUser.getCart().getCartDetails().get(0));
        userService.saveOrUpdate(savedUser);

        assertThat(savedUser.getCart().getCartDetails(), hasSize(1));
    }
}
