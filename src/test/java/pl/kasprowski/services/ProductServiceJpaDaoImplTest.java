package pl.kasprowski.services;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.configurations.JPATestConfiguration;
import pl.kasprowski.domain.Product;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JPATestConfiguration.class)
@ActiveProfiles("jpadao")
public class ProductServiceJpaDaoImplTest {

    @Autowired
    private ProductService productService;

    @Test
    public void testGetAll() throws Exception {
        List<Product> products = (List<Product>) productService.getAll();

        assertThat(products, hasSize(4));
    }

    @Test
    public void testGetById() throws Exception {
        Product product = productService.getById(1);

        assertThat(product, is(instanceOf(Product.class)));
        assertThat(product.getDescription(), is(equalTo("Product1")));
        assertThat(product.getPrice(), closeTo(new BigDecimal(12.00), new BigDecimal(0.005)));
        assertThat(product.getImageUrl(), is(equalTo("http://example/product/1")));
    }

    @Test
    public void testSaveOrUpdate() throws Exception {
        String description = "description test";
        String imageUrl = "http://test.com";
        BigDecimal price = new BigDecimal("100.00");

        Product product = new Product();
        product.setDescription(description);
        product.setImageUrl(imageUrl);
        product.setPrice(price);

        Product returnedProduct = productService.saveOrUpdate(product);

        assertThat(returnedProduct, is(instanceOf(Product.class)));
        assertThat(returnedProduct.getDescription(), is(equalTo("description test")));
        assertThat(returnedProduct.getPrice(), closeTo(new BigDecimal(100.00), new BigDecimal(0.005)));
        assertThat(returnedProduct.getImageUrl(), is(equalTo("http://test.com")));
        assertThat(returnedProduct.getId(), is(equalTo(5)));

        productService.delete(returnedProduct.getId());
    }

    @Test
    @Ignore
    public void testDelete() throws Exception {
        Product productToBeDeleted = productService.getById(1);
        productService.delete(productToBeDeleted.getId());
        List<Product> products = (List<Product>) productService.getAll();

        assertThat(products, hasSize(3));

        productService.saveOrUpdate(productToBeDeleted);
    }
}
