package pl.kasprowski.services;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.configurations.JPATestConfiguration;
import pl.kasprowski.domain.Customer;
import pl.kasprowski.domain.User;

import javax.validation.constraints.NotNull;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JPATestConfiguration.class)
@ActiveProfiles("jpadao")
public class CustomerServiceJpaDaoImplTest {

    @Autowired
    private CustomerService customerService;

    @Test
    public void testGetAll() throws Exception {
        List<Customer> customers = (List<Customer>) customerService.getAll();

        assertThat(customers, hasSize(3));
    }
    @Test
    public void testGetById() throws Exception{
        Customer customer = customerService.getById(1);

        assertThat(customer, is(instanceOf(Customer.class)));
        assertThat(customer.getFirstName(), is(equalTo("name1")));
        assertThat(customer.getZipCode(), is(equalTo("1111")));
    }

    @Test
    public void testSaveOrUpdate() throws Exception {
        String name = "testName";
        String surname = "testSurname";
        String addressOne = "address1";
        String addressTwo = "address2";
        String city = "city";
        String email = "test@test";
        String state = "state";
        String zipCode = "11-1111";
        String phoneNumber = "111111111";

        Customer customer = new Customer();
        customer.setFirstName(name);
        customer.setLastName(surname);
        customer.setAddressOne(addressOne);
        customer.setAddressTwo(addressTwo);
        customer.setCity(city);
        customer.setEmail(email);
        customer.setState(state);
        customer.setZipCode(zipCode);
        customer.setPhoneNumber(phoneNumber);

        Customer returnedCustomer = customerService.saveOrUpdate(customer);

        assertThat(returnedCustomer, is(instanceOf(Customer.class)));
        assertThat(returnedCustomer.getPhoneNumber(), is(equalTo("111111111")));
        assertThat(returnedCustomer.getEmail(), is(equalTo("test@test")));
        assertThat(returnedCustomer.getVersion(), is(equalTo(0)));

        customerService.delete(returnedCustomer.getId());
    }

    @Test
    @Ignore
    public void testDelete() throws Exception {
        Customer customerToBeDeleted = customerService.getById(2);
        customerService.delete(customerToBeDeleted.getId());
        List<Customer> customers = (List<Customer>) customerService.getAll();

        assertThat(customers, hasSize(2));

        customerService.saveOrUpdate(customerToBeDeleted);
    }

    @Test
    public void testSaveWithUser() throws Exception {
        Customer customer = new Customer();
        User user = new User();
        user.setUsername("userName");
        user.setPassword("password");
        customer.setUser(user);

        Customer savedCustomer = customerService.saveOrUpdate(customer);

        assertThat(savedCustomer.getUser().getId(), is(notNullValue()));

        customerService.delete(savedCustomer.getId());
    }
}
