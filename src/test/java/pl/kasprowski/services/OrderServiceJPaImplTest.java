package pl.kasprowski.services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.configurations.JPATestConfiguration;
import pl.kasprowski.domain.OrderDetails;
import pl.kasprowski.domain.OrderEntity;
import pl.kasprowski.domain.OrderStatus;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JPATestConfiguration.class)
@ActiveProfiles("jpadao")
public class OrderServiceJPaImplTest {

    @Autowired
    private OrderService orderService;

    @Test
    public void testGetAll() throws Exception {
        List<OrderEntity> orders = (List<OrderEntity>) orderService.getAll();

        assertThat(orders, hasSize(3));
    }

    @Test
    public void testGetById() throws Exception {
        OrderEntity order = orderService.getById(2);

        assertThat(order, is(instanceOf(OrderEntity.class)));
        assertThat(order.getStatus(), is(equalTo(OrderStatus.NEW)));
    }

    @Test
    public void testSaveOrUpdate() throws Exception {
        String shippingAddress = "test address";
        OrderStatus status = OrderStatus.NEW;

        OrderEntity order = new OrderEntity();
        order.setStatus(status);
        order.setShippingAddress(shippingAddress);

        OrderEntity returnedOrder = orderService.saveOrUpdate(order);

        assertThat(returnedOrder, is(instanceOf(OrderEntity.class)));
        assertThat(returnedOrder.getId(), is(notNullValue()));
        assertThat(returnedOrder.getDateCreated(), is(notNullValue()));
        assertThat(returnedOrder.getDateUpdated(), is(notNullValue()));
        assertThat(returnedOrder.getStatus(), is(equalTo(status)));
        assertThat(returnedOrder.getShippingAddress(), is(equalTo(shippingAddress)));

        orderService.delete(returnedOrder.getId());
    }

    @Test
    public void testDelete() throws Exception {
        OrderEntity order = orderService.getById(1);
        orderService.delete(order.getId());
        List<OrderEntity> orders = (List<OrderEntity>) orderService.getAll();

        assertThat(orders, hasSize(2));

        orderService.saveOrUpdate(order);
    }

    @Test
    public void testSaveWithDetails() throws Exception {
        String shippingAddress = "test";
        OrderStatus status = OrderStatus.NEW;

        OrderEntity order = new OrderEntity();
        order.setStatus(status);
        order.setShippingAddress(shippingAddress);

        OrderDetails detail1 = new OrderDetails();
        OrderDetails detail2 = new OrderDetails();

        order.addOrderDetails(detail1);
        order.addOrderDetails(detail2);

        OrderEntity returnedOrder = orderService.saveOrUpdate(order);

        assertThat(returnedOrder, is(instanceOf(OrderEntity.class)));
        assertThat(returnedOrder.getId(), is(notNullValue()));
        assertThat(returnedOrder.getOrderDetails(), hasSize(2));

        returnedOrder.removeOrderDetails(returnedOrder.getOrderDetails().get(0));
        orderService.saveOrUpdate(returnedOrder);

        assertThat(returnedOrder.getOrderDetails(), hasSize(1));

        orderService.delete(returnedOrder.getId());
    }
}
