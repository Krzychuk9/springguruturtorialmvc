package pl.kasprowski.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.commands.CustomerForm;
import pl.kasprowski.domain.Customer;
import pl.kasprowski.domain.User;
import pl.kasprowski.services.CustomerService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class CustomerControllerTest {

    @Mock
    private CustomerService customerService;

    @InjectMocks
    private CustomerController customerController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
    }

    @Test
    public void showCustomersList() throws Exception {
        List<Customer> customers = new ArrayList<>();
        customers.add(new Customer());
        customers.add(new Customer());
        customers.add(new Customer());

        when(customerService.getAll()).thenReturn((List) customers);

        mockMvc.perform(get("/customers")).
                andExpect(status().isOk()).
                andExpect(view().name("customer/customers")).
                andExpect(model().attribute("customers", hasSize(3)));
    }

    @Test
    public void showCustomerById() throws Exception {
        Integer id = 2;

        when(customerService.getById(id)).thenReturn(new Customer());

        mockMvc.perform(get("/customer/2")).
                andExpect(status().isOk()).
                andExpect(view().name("customer/customer")).
                andExpect(model().attribute("customer", instanceOf(Customer.class)));
    }

    @Test
    public void addNewCustomer() throws Exception {
        verifyZeroInteractions(customerService);

        mockMvc.perform(get("/customer/new")).
                andExpect(status().isOk()).
                andExpect(view().name("customer/customerform")).
                andExpect(model().attribute("customer", instanceOf(CustomerForm.class)));
    }

    @Test
    public void updateCustomer() throws Exception {
        Integer id = 2;

        when(customerService.getById(id)).thenReturn(new Customer());

        mockMvc.perform(get("/customer/update/2")).
                andExpect(status().isOk()).
                andExpect(view().name("customer/customerform")).
                andExpect(model().attribute("customer", instanceOf(Customer.class)));
    }

    @Test
    public void addOrUpdateCustomer() throws Exception {
        Integer id = 1;
        String firstName = "testName";
        String lastName = "testSurname";
        String email = "test@test";
        String phoneNumber = "123";
        String addressOne = "testAddress1";
        String addressTwo = "testAddress2";
        String city = "testCity";
        String state = "testState";
        String zipCode = "testZipCode";
        String username = "username";
        String password= "password";

        Customer returnedCustomer = new Customer(id, firstName, lastName, email, phoneNumber, addressOne, addressTwo, city, state, zipCode);
        returnedCustomer.setUser(new User());
        returnedCustomer.getUser().setUsername(username);
        returnedCustomer.getUser().setPassword(password);

        when(customerService.saveOrUpdateCustomerForm(org.mockito.Mockito.any(CustomerForm.class))).thenReturn(returnedCustomer);
        when(customerService.getById(org.mockito.Mockito.any(Integer.class))).thenReturn(returnedCustomer);

        mockMvc.perform(post("/customer").
                param("customerId", id.toString()).
                param("userName",username).
                param("passwordText",password).param("passwordTextConf", password).
                param("firstName", firstName).
                param("lastName", lastName).
                param("email", email).
                param("phoneNumber", phoneNumber).
                param("addressOne", addressOne).
                param("addressTwo", addressTwo).
                param("city", city).
                param("state", state).
                param("zipCode", zipCode)).andExpect(status().is3xxRedirection()).
                andExpect(view().name("redirect:/customer/1"));

        ArgumentCaptor<CustomerForm> CustomerCaptor = ArgumentCaptor.forClass(CustomerForm.class);
        verify(customerService).saveOrUpdateCustomerForm(CustomerCaptor.capture());

        assertThat(CustomerCaptor.getValue().getCustomerId(), is(id));
        assertThat(CustomerCaptor.getValue().getFirstName(), is(firstName));
        assertThat(CustomerCaptor.getValue().getLastName(), is(lastName));
        assertThat(CustomerCaptor.getValue().getEmail(), is(email));
        assertThat(CustomerCaptor.getValue().getPhoneNumber(), is(phoneNumber));
    }

    @Test
    public void deleteCustomer() throws Exception {
        Integer id = 2;

        mockMvc.perform(get("/customer/delete/2")).
                andExpect(status().is3xxRedirection()).
                andExpect(view().name("redirect:/customers"));

        verify(customerService, times(1)).delete(id);
    }
}