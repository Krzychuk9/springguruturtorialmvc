package pl.kasprowski.controllers;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.commands.ProductForm;
import pl.kasprowski.domain.Product;
import pl.kasprowski.services.ProductService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ProductControllerTest {

    @Mock
    private ProductService productService;

    @InjectMocks
    private ProductController productController;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
    }

    @Test
    public void showProducts() throws Exception {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product());
        productList.add(new Product());

        when(productService.getAll()).thenReturn((List) productList);

        mockMvc.perform(get("/products")).andExpect(status().isOk()).
                andExpect(view().name("product/products")).
                andExpect(model().attribute("products", hasSize(2)));
    }

    @Test
    public void showProductById() throws Exception {
        Integer id = 1;

        when(productService.getById(id)).thenReturn(new Product());

        mockMvc.perform(get("/product/1")).andExpect(status().isOk()).
                andExpect(view().name("product/product")).
                andExpect(model().attribute("product", instanceOf(Product.class)));
    }

    @Test
    public void updateProduct() throws Exception {
        Integer id = 1;

        when(productService.getById(id)).thenReturn(new Product());

        mockMvc.perform(get("/product/update/1")).andExpect(status().isOk()).
                andExpect(view().name("product/productform")).
                andExpect(model().attribute("productForm", instanceOf(Product.class)));
    }

    @Test
    public void showProductForm() throws Exception {

        verifyZeroInteractions(productService);

        mockMvc.perform(get("/product/new")).andExpect(status().isOk()).
                andExpect(view().name("product/productform")).
                andExpect(model().attribute("productForm", instanceOf(ProductForm.class)));
    }

    @Test
    public void addOrUpdateProduct() throws Exception {
        Integer id = 1;
        String description = "Test description";
        BigDecimal price = new BigDecimal("12.00");
        String imageUrl = "http://example.com";

        ProductForm returnProduct = new ProductForm();
        returnProduct.setId(id);
        returnProduct.setDescription(description);
        returnProduct.setPrice(price);
        returnProduct.setImageUrl(imageUrl);

        when(productService.saveOrUpdateProductForm(org.mockito.Mockito.any(ProductForm.class))).thenReturn(returnProduct);

        mockMvc.perform(post("/product").param("id", "1").
                param("description", description).
                param("price", "12.00").param("imageUrl", imageUrl)).
                andExpect(status().is3xxRedirection()).
                andExpect(model().attribute("productForm", instanceOf(ProductForm.class))).
                andExpect(model().attribute("productForm", hasProperty("id", is(id)))).
                andExpect(model().attribute("productForm", hasProperty("description", is(description)))).
                andExpect(model().attribute("productForm", hasProperty("price", is(price)))).
                andExpect(model().attribute("productForm", hasProperty("imageUrl", is(imageUrl))));

        ArgumentCaptor<ProductForm> boundProduct = ArgumentCaptor.forClass(ProductForm.class);
        verify(productService).saveOrUpdateProductForm(boundProduct.capture());

        assertEquals(id, boundProduct.getValue().getId());
        assertEquals(description, boundProduct.getValue().getDescription());
        assertEquals(price, boundProduct.getValue().getPrice());
        assertEquals(imageUrl, boundProduct.getValue().getImageUrl());
    }

    @Test
    public void deleteProduct() throws Exception {
        Integer id = 1;

        mockMvc.perform(get("/product/delete/1")).andExpect(status().is3xxRedirection()).
                andExpect(view().name("redirect:/products"));

        verify(productService, times(1)).delete(id);
    }
}