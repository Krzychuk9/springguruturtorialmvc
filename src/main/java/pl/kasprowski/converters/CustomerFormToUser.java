package pl.kasprowski.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import pl.kasprowski.commands.CustomerForm;
import pl.kasprowski.domain.Customer;
import pl.kasprowski.domain.User;

@Component
public class CustomerFormToUser implements Converter<CustomerForm, Customer>{

    @Override
    public Customer convert(CustomerForm customerForm) {
        Customer c = new Customer();
        c.setUser(new User());
        c.getUser().setId(customerForm.getUserId());
        c.getUser().setVersion(customerForm.getUserVersion());
        c.setId(customerForm.getCustomerId());
        c.setVersion(customerForm.getCustomerVersion());
        c.getUser().setUsername(customerForm.getUserName());
        c.getUser().setPassword(customerForm.getPasswordText());
        c.setFirstName(customerForm.getFirstName());
        c.setLastName(customerForm.getLastName());
        c.setEmail(customerForm.getEmail());
        c.setPhoneNumber(customerForm.getPhoneNumber());

        return c;
    }
}
