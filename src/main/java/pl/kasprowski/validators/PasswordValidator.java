package pl.kasprowski.validators;

import org.springframework.stereotype.Component;

@Component
public class PasswordValidator {

    public boolean validatePassword(String password, String conf) {
        return password.equals(conf);
    }
}
