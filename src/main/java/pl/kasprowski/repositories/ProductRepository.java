package pl.kasprowski.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.domain.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {
}
