package pl.kasprowski.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.domain.OrderEntity;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Integer>{
}
