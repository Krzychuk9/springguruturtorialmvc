package pl.kasprowski.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.domain.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
}
