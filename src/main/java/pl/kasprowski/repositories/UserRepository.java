package pl.kasprowski.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
}
