package pl.kasprowski.services;

import pl.kasprowski.domain.OrderEntity;

public interface OrderService extends CRUDService<OrderEntity>{
}
