package pl.kasprowski.services;

import pl.kasprowski.commands.CustomerForm;
import pl.kasprowski.domain.Customer;

public interface CustomerService extends CRUDService<Customer> {

    Customer saveOrUpdateCustomerForm(CustomerForm customerForm);
}
