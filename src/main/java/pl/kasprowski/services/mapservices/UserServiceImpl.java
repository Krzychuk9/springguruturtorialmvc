package pl.kasprowski.services.mapservices;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.DomainObject;
import pl.kasprowski.domain.User;
import pl.kasprowski.services.UserService;

import java.util.List;

@Service
@Profile("map")
public class UserServiceImpl extends AbstractMapService implements UserService{
    @Override
    public List<DomainObject> getAll() {
        return super.getAll();
    }

    @Override
    public User getById(Integer id) {
        return (User) super.getById(id);
    }

    @Override
    public User saveOrUpdate(User user) {
        return (User) super.saveOrUpdate(user);
    }

    @Override
    public void delete(Integer id) {
        super.delete(id);
    }
}
