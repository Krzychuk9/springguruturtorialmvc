package pl.kasprowski.services.mapservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.commands.ProductForm;
import pl.kasprowski.converters.ProductFormToProduct;
import pl.kasprowski.converters.ProductToProductForm;
import pl.kasprowski.domain.DomainObject;
import pl.kasprowski.domain.Product;
import pl.kasprowski.services.ProductService;

import java.util.List;

@Service
@Profile("map")
public class ProductServiceImpl extends AbstractMapService implements ProductService {

    @Autowired
    private ProductFormToProduct productFormToProduct;

    @Autowired
    private ProductToProductForm productToProductForm;

    @Override
    public List<DomainObject> getAll() {
        return super.getAll();
    }

    @Override
    public Product getById(Integer id) {
        return (Product) super.getById(id);
    }

    @Override
    public Product saveOrUpdate(Product product) {
        return (Product) super.saveOrUpdate(product);
    }

    @Override
    public void delete(Integer id) {
        super.delete(id);
    }

    @Override
    public ProductForm saveOrUpdateProductForm(ProductForm productForm) {

        Product product = productFormToProduct.convert(productForm);
        Product savedProduct = saveOrUpdate(product);
        ProductForm savedProductForm = productToProductForm.convert(savedProduct);

        return savedProductForm;
    }
}
