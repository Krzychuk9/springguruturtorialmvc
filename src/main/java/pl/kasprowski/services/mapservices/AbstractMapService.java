package pl.kasprowski.services.mapservices;

import pl.kasprowski.domain.DomainObject;

import java.util.*;

public abstract class AbstractMapService {
    protected HashMap<Integer, DomainObject> domainMap;

    public AbstractMapService() {
        domainMap = new HashMap<>();
    }

    public List<DomainObject> getAll() {
        return new ArrayList<>(domainMap.values());
    }

    public DomainObject getById(Integer id) {
        return domainMap.get(id);
    }

    public DomainObject saveOrUpdate(DomainObject domainObject) {
        if (domainObject != null) {
            if (domainObject.getId() == null) {
                domainObject.setId(this.nextId());
            }
            domainMap.put(domainObject.getId(), domainObject);
            return domainObject;
        } else {
            throw new RuntimeException("DomainObject can not be null");
        }
    }

    public void delete(Integer id) {
        this.domainMap.remove(id);
    }

    private Integer nextId() {
        Set<Integer> keySet = domainMap.keySet();
        if (keySet.size() == 0) {
            return 1;
        } else {
            return Collections.max(domainMap.keySet()) + 1;
        }
    }
}
