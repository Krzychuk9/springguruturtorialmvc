package pl.kasprowski.services;

import java.util.List;

public interface CRUDService<T> {

    List<?> getAll();

    T getById(Integer id);

    T saveOrUpdate(T t);

    void delete(Integer id);
}
