package pl.kasprowski.services.reposervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.OrderEntity;
import pl.kasprowski.repositories.OrderRepository;
import pl.kasprowski.services.OrderService;

import java.util.ArrayList;
import java.util.List;

@Service
@Profile("springdatajpa")
public class OrderServiceRepoImpl implements OrderService{

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<OrderEntity> getAll() {
        List<OrderEntity> orders = new ArrayList<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    @Override
    public OrderEntity getById(Integer id) {
        return orderRepository.findOne(id);
    }

    @Override
    public OrderEntity saveOrUpdate(OrderEntity orderEntity) {
        return orderRepository.save(orderEntity);
    }

    @Override
    public void delete(Integer id) {
        orderRepository.delete(id);
    }
}
