package pl.kasprowski.services.reposervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.commands.ProductForm;
import pl.kasprowski.converters.ProductFormToProduct;
import pl.kasprowski.converters.ProductToProductForm;
import pl.kasprowski.domain.Product;
import pl.kasprowski.repositories.ProductRepository;
import pl.kasprowski.services.ProductService;

import java.util.ArrayList;
import java.util.List;

@Service
@Profile("springdatajpa")
public class ProductServiceRepoImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductFormToProduct productFormToProduct;

    @Autowired
    private ProductToProductForm productToProductForm;

    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);
        return products;
    }

    @Override
    public Product getById(Integer id) {
        return productRepository.findOne(id);
    }

    @Override
    public Product saveOrUpdate(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void delete(Integer id) {
        productRepository.delete(id);
    }

    @Override
    public ProductForm saveOrUpdateProductForm(ProductForm productForm) {

        Product product = productFormToProduct.convert(productForm);
        Product savedProduct = saveOrUpdate(product);
        ProductForm savedProductForm = productToProductForm.convert(savedProduct);

        return savedProductForm;
    }
}
