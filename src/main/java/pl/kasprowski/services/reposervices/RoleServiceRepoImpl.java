package pl.kasprowski.services.reposervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.Role;
import pl.kasprowski.repositories.RoleRepository;
import pl.kasprowski.services.RoleService;

import java.util.ArrayList;
import java.util.List;

@Service
@Profile("springdatajpa")
public class RoleServiceRepoImpl implements RoleService {

    @Autowired
    private RoleRepository repo;

    @Override
    public List<Role> getAll() {
        List<Role> roles = new ArrayList<>();
        repo.findAll().forEach(roles::add);
        return roles;
    }

    @Override
    public Role getById(Integer id) {
        return repo.findOne(id);
    }

    @Override
    public Role saveOrUpdate(Role role) {
        return repo.save(role);
    }

    @Override
    public void delete(Integer id) {
        repo.delete(id);
    }
}
