package pl.kasprowski.services;

import pl.kasprowski.commands.ProductForm;
import pl.kasprowski.domain.Product;

public interface ProductService extends CRUDService<Product> {
    ProductForm saveOrUpdateProductForm(ProductForm productForm);
}
