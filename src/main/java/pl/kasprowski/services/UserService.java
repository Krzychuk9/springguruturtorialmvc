package pl.kasprowski.services;

import pl.kasprowski.domain.User;

public interface UserService extends CRUDService<User>{
}
