package pl.kasprowski.services;

import pl.kasprowski.domain.Role;

public interface RoleService extends CRUDService<Role>{
}
