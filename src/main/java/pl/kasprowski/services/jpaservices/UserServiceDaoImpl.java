package pl.kasprowski.services.jpaservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.User;
import pl.kasprowski.services.security.EncryptionService;
import pl.kasprowski.services.UserService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class UserServiceDaoImpl implements UserService{

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private EncryptionService encryptionService;

    @Override
    public List<User> getAll() {
        EntityManager em = emf.createEntityManager();

        return em.createQuery("FROM User",User.class).getResultList();
    }

    @Override
    public User getById(Integer id) {
        EntityManager em = emf.createEntityManager();

        return em.find(User.class, id);
    }

    @Override
    public User saveOrUpdate(User user) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        if(user.getPassword() != null){
            user.setEncryptedPassword(encryptionService.encryptString(user.getPassword()));
        }

        User returnedUser = em.merge(user);
        em.getTransaction().commit();

        return returnedUser;
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(User.class,id));
        em.getTransaction().commit();
    }
}
