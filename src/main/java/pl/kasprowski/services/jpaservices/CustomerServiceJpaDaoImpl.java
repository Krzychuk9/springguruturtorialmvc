package pl.kasprowski.services.jpaservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.commands.CustomerForm;
import pl.kasprowski.converters.CustomerFormToUser;
import pl.kasprowski.domain.Customer;
import pl.kasprowski.services.CustomerService;
import pl.kasprowski.services.security.EncryptionService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class CustomerServiceJpaDaoImpl implements CustomerService {

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private CustomerFormToUser customerFormToUser;

    @Autowired
    private EncryptionService encryptionService;

    @Override
    public List<Customer> getAll() {
        EntityManager em = emf.createEntityManager();

        return em.createQuery("FROM Customer", Customer.class).getResultList();
    }

    @Override
    public Customer getById(Integer id) {
        EntityManager em = emf.createEntityManager();

        return em.find(Customer.class, id);
    }

    @Override
    public Customer saveOrUpdate(Customer customer) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();

        if(customer.getUser() != null && customer.getUser().getPassword() != null){
            customer.getUser().setEncryptedPassword(encryptionService.encryptString(customer.getUser().getPassword()));
        }

        Customer returnedCustomer = em.merge(customer);
        em.getTransaction().commit();

        return returnedCustomer;
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Customer customer = em.find(Customer.class, id);
        em.remove(customer);
        em.getTransaction().commit();
    }

    @Override
    public Customer saveOrUpdateCustomerForm(CustomerForm customerForm) {
        Customer newCustomer = customerFormToUser.convert(customerForm);
        if(newCustomer.getUser().getId() != null){
            Customer existingCustomer = getById(newCustomer.getUser().getId());

            newCustomer.getUser().setEnabled(existingCustomer.getUser().getEnabled());
        }

        return saveOrUpdate(newCustomer);
    }
}
