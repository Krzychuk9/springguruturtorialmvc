package pl.kasprowski.services.jpaservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.commands.ProductForm;
import pl.kasprowski.converters.ProductFormToProduct;
import pl.kasprowski.converters.ProductToProductForm;
import pl.kasprowski.domain.Product;
import pl.kasprowski.services.ProductService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class ProductServiceJpaDaoImpl implements ProductService {

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private ProductFormToProduct productFormToProduct;

    @Autowired
    private ProductToProductForm productToProductForm;

    @Override
    public List<Product> getAll() {
        EntityManager em = emf.createEntityManager();

        return em.createQuery("FROM Product", Product.class).getResultList();
    }

    @Override
    public Product getById(Integer id) {
        EntityManager em = emf.createEntityManager();

        return em.find(Product.class, id);
    }

    @Override
    public Product saveOrUpdate(Product product) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Product returnedProduct = em.merge(product);
        em.getTransaction().commit();

        return returnedProduct;
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Product product = em.find(Product.class, id);
        em.remove(product);
        em.getTransaction().commit();
    }

    @Override
    public ProductForm saveOrUpdateProductForm(ProductForm productForm) {

        Product product = productFormToProduct.convert(productForm);
        Product savedProduct = saveOrUpdate(product);
        ProductForm savedProductForm = productToProductForm.convert(savedProduct);

        return savedProductForm;
    }
}
