package pl.kasprowski.services.jpaservices;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.OrderEntity;
import pl.kasprowski.services.OrderService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class OrderServiceJpaDaoImpl implements OrderService{

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Override
    public List<OrderEntity> getAll() {
        EntityManager em = emf.createEntityManager();

        return em.createQuery("FROM OrderEntity", OrderEntity.class).getResultList();
    }

    @Override
    public OrderEntity getById(Integer id) {
        EntityManager em = emf.createEntityManager();

        return em.find(OrderEntity.class, id);
    }

    @Override
    public OrderEntity saveOrUpdate(OrderEntity orderEntity) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        OrderEntity returnedOrder = em.merge(orderEntity);
        em.getTransaction().commit();

        return returnedOrder;
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(OrderEntity.class, id));
        em.getTransaction().commit();
    }
}
