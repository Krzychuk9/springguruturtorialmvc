package pl.kasprowski.services.jpaservices;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import pl.kasprowski.domain.Role;
import pl.kasprowski.services.RoleService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Service
@Profile("jpadao")
public class RoleServiceJpaDaoImpl implements RoleService {

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Override
    public List<Role> getAll() {
        EntityManager em = emf.createEntityManager();

        return em.createQuery("FROM Role", Role.class).getResultList();
    }

    @Override
    public Role getById(Integer id) {
        EntityManager em = emf.createEntityManager();

        return em.find(Role.class, id);
    }

    @Override
    public Role saveOrUpdate(Role role) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Role returnedRole = em.merge(role);
        em.getTransaction().commit();
        return returnedRole;
    }

    @Override
    public void delete(Integer id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        em.remove(em.find(Role.class, id));
        em.getTransaction().commit();
    }
}
