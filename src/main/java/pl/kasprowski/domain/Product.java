package pl.kasprowski.domain;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class Product extends AbstractDomainClass {

    private String description;
    private BigDecimal price;
    private String imageUrl;

    public Product() {
    }

    public Product(Integer id, String description, BigDecimal price, String imageUrl) {
        this.setId(id);
        this.description = description;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
