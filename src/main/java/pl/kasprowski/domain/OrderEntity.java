package pl.kasprowski.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class OrderEntity extends AbstractDomainClass {

    @OneToOne
    private Customer customer;

    @OneToMany(mappedBy = "orderEntity", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<OrderDetails> orderDetails = new ArrayList<>();

    private OrderStatus status;
    private String shippingAddress;
    @Temporal(TemporalType.DATE)
    private Date dateShipped;

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public List<OrderDetails> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDateShipped() {
        return dateShipped;
    }

    public void setDateShipped(Date dateShipped) {
        this.dateShipped = dateShipped;
    }

    public void addOrderDetails(OrderDetails orderDetail) {
        orderDetails.add(orderDetail);
        orderDetail.setOrderEntity(this);
    }

    public void removeOrderDetails(OrderDetails orderDetail) {
        orderDetail.setOrderEntity(null);
        orderDetails.remove(orderDetail);
    }
}
