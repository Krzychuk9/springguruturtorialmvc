package pl.kasprowski.domain;


public enum OrderStatus {
    NEW, ALLOCATED, SHIPPED
}
