package pl.kasprowski.domain;

public interface DomainObject {
    Integer getId();

    void setId(Integer id);
}
