package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.kasprowski.commands.CustomerForm;
import pl.kasprowski.domain.Customer;
import pl.kasprowski.services.CustomerService;
import pl.kasprowski.validators.CustomerFormValidator;
import pl.kasprowski.validators.PasswordValidator;

import javax.validation.Valid;

@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerFormValidator passwordValidator;

    @RequestMapping("/customers")
    public String showCustomersList(Model model) {
        model.addAttribute("customers", customerService.getAll());
        return "customer/customers";
    }

    @RequestMapping("/customer/{id}")
    private String showCustomerById(@PathVariable Integer id, Model model) {
        model.addAttribute("customer", customerService.getById(id));
        return "customer/customer";
    }

    @RequestMapping("/customer/new")
    public String addNewCustomer(Model model) {
        model.addAttribute("customerForm", new CustomerForm());
        return "customer/customerform";
    }

    @RequestMapping("/customer/update/{id}")
    public String updateCustomer(@PathVariable Integer id, Model model) {
        model.addAttribute("customerForm", customerService.getById(id));
        return "customer/customerform";
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public String addOrUpdateCustomer(@Valid CustomerForm customerForm, BindingResult result) {

        passwordValidator.validate(customerForm, result);

        if(result.hasErrors()){
            return "customer/customerform";
        }

        Customer savedCustomer = customerService.saveOrUpdateCustomerForm(customerForm);
        return "redirect:/customer/" + savedCustomer.getId();
    }

    @RequestMapping("customer/delete/{id}")
    public String deleteCustomer(@PathVariable Integer id){
        customerService.delete(id);
        return "redirect:/customers";
    }
}
