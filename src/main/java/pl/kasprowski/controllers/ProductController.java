package pl.kasprowski.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.kasprowski.commands.ProductForm;
import pl.kasprowski.domain.Product;
import pl.kasprowski.services.ProductService;

import javax.validation.Valid;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/products")
    public String showProducts(Model model) {
        model.addAttribute("products", productService.getAll());
        return "product/products";
    }

    @RequestMapping("/product/{id}")
    public String showProductById(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("product", productService.getById(id));
        return "product/product";
    }

    @RequestMapping("/product/update/{id}")
    public String updateProduct(@PathVariable Integer id, Model model) {
        model.addAttribute("productForm", productService.getById(id));
        return "product/productform";
    }

    @RequestMapping("/product/new")
    public String showProductForm(Model model) {
        model.addAttribute("productForm", new ProductForm());
        return "product/productform";
    }

    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public String addOrUpdateProduct(@Valid ProductForm productForm, BindingResult result) {

        if (result.hasErrors()) {
            return "product/productform";
        }

        ProductForm savedProduct = productService.saveOrUpdateProductForm(productForm);
        return "redirect:/product/" + savedProduct.getId();
    }

    @RequestMapping("/product/delete/{id}")
    public String deleteProduct(@PathVariable Integer id) {
        productService.delete(id);
        return "redirect:/products";
    }
}
