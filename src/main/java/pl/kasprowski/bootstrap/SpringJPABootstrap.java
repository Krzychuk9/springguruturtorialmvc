package pl.kasprowski.bootstrap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.kasprowski.domain.*;
import pl.kasprowski.services.*;

import java.math.BigDecimal;
import java.util.List;

@Component
public class SpringJPABootstrap implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private ProductService productService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        loadProducts();
        loadCustomers();
        loadOrders();
        loadRoles();
        assignUsersToDefaultRole();
    }

    private void assignUsersToDefaultRole() {
        List<Role> roles = (List<Role>) roleService.getAll();
        List<User> users = (List<User>) userService.getAll();

        roles.forEach(role -> {
            if (role.getRole().equalsIgnoreCase("CUSTOMER")) {
                users.forEach(user -> {
                    user.addRole(role);
                    User user1 = userService.saveOrUpdate(user);
                });
            }
        });

    }

    private void loadRoles() {
        Role role = new Role();
        role.setRole("CUSTOMER");
        roleService.saveOrUpdate(role);
    }

    private void loadCustomers() {
        List<Product> products = (List<Product>) productService.getAll();

        Customer customer1 = new Customer();
        User user1 = new User();
        user1.setUsername("user1");
        user1.setPassword("user1");

        Cart cart1 = new Cart();
        cart1.setUser(user1);

        CartDetails details1 = new CartDetails();
        details1.setProduct(products.get(0));
        details1.setQuantity(2);
        cart1.addCartDetails(details1);
        CartDetails details2 = new CartDetails();
        details2.setProduct(products.get(2));
        details2.setQuantity(4);
        cart1.addCartDetails(details2);

        user1.setCart(cart1);
        user1.setCustomer(customer1);
        customer1.setFirstName("name1");
        customer1.setLastName("surname1");
        customer1.setAddressOne("address11");
        customer1.setAddressTwo("address12");
        customer1.setCity("city1");
        customer1.setEmail("email1@email");
        customer1.setState("state1");
        customer1.setZipCode("1111");
        customer1.setPhoneNumber("1111");
        customerService.saveOrUpdate(customer1);

        Customer customer2 = new Customer();
        User user2 = new User();
        user2.setUsername("user2");
        user2.setPassword("user2");

        Cart cart2 = new Cart();
        cart2.setUser(user2);

        CartDetails details3 = new CartDetails();
        details3.setProduct(products.get(1));
        details3.setQuantity(9);
        cart2.addCartDetails(details3);
        CartDetails details4 = new CartDetails();
        details4.setProduct(products.get(2));
        details4.setQuantity(2);
        cart2.addCartDetails(details4);

        user2.setCart(cart2);
        user2.setCustomer(customer2);
        customer2.setFirstName("name2");
        customer2.setLastName("surname2");
        customer2.setAddressOne("address21");
        customer2.setAddressTwo("address22");
        customer2.setCity("city2");
        customer2.setEmail("email2@email");
        customer2.setState("state2");
        customer2.setZipCode("2222");
        customer2.setPhoneNumber("2222");
        customerService.saveOrUpdate(customer2);

        Customer customer3 = new Customer();
        User user3 = new User();
        user3.setUsername("user3");
        user3.setPassword("user3");

        Cart cart3 = new Cart();
        cart3.setUser(user3);

        CartDetails details5 = new CartDetails();
        details5.setProduct(products.get(0));
        details5.setQuantity(5);
        cart3.addCartDetails(details5);
        CartDetails details6 = new CartDetails();
        details6.setProduct(products.get(1));
        details6.setQuantity(3);
        cart3.addCartDetails(details6);

        user3.setCart(cart3);
        user3.setCustomer(customer3);
        customer3.setFirstName("name3");
        customer3.setLastName("surname3");
        customer3.setAddressOne("address31");
        customer3.setAddressTwo("address32");
        customer3.setCity("city3");
        customer3.setEmail("email3@email");
        customer3.setState("state3");
        customer3.setZipCode("3333");
        customer3.setPhoneNumber("3333");
        customerService.saveOrUpdate(customer3);
    }

    private void loadProducts() {
        Product product1 = new Product();
        product1.setDescription("Product1");
        product1.setPrice(new BigDecimal("12.00"));
        product1.setImageUrl("http://example/product/1");
        productService.saveOrUpdate(product1);

        Product product2 = new Product();
        product2.setDescription("Product2");
        product2.setPrice(new BigDecimal("32.12"));
        product2.setImageUrl("http://example/product/2");
        productService.saveOrUpdate(product2);

        Product product3 = new Product();
        product3.setDescription("Product3");
        product3.setPrice(new BigDecimal("4.32"));
        product3.setImageUrl("http://example/product/3");
        productService.saveOrUpdate(product3);

        Product product4 = new Product();
        product4.setDescription("Product4");
        product4.setPrice(new BigDecimal("23.45"));
        product4.setImageUrl("http://example/product/4");
        productService.saveOrUpdate(product4);
    }

    private void loadOrders() {
        List<Customer> customers = (List<Customer>) customerService.getAll();
        List<Product> products = (List<Product>) productService.getAll();

        customers.stream().forEach(c -> {
            OrderEntity order = new OrderEntity();
            order.setShippingAddress("shippingAddress1");
            order.setStatus(OrderStatus.NEW);
            OrderDetails orderDetails1 = new OrderDetails();
            orderDetails1.setQuantity(2);
            orderDetails1.setProduct(products.get(1));
            order.addOrderDetails(orderDetails1);
            order.setCustomer(c);
            orderService.saveOrUpdate(order);
        });
    }
}
